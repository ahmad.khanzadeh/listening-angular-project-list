export interface UserDataModel{
page: number,
'per-page': number,
total:number,
'total-page':number,
data: UserModel[],
interface: UserSupport


}
export interface UserModel{
    id: number,
    email: string,
    'first-name': string,
    'last-name': string,
    avatar: string

}
export interface UserSupport{
    'url': any,
    'text': any,
}