import { environment } from './../environments/environment';
// import { Observable } from 'rxjs/observable';
import { UserDataModel } from './model/user.model';
import { Injectable } from '@angular/core';
//  import {DataService} from './services/data.services';
import{HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
// import{UserDataModel} from './model/user.model',
import{Observable, throwError} from 'rxjs';
import{catchError, retry} from 'rxjs/operators';
import { Button } from 'protractor';

@Injectable({
  providedIn: 'root'
})
// export class UserListService extends DataService{
  export class UserListService {
 private userUrl: "/api/users";
  httpOptions={
    
    headers: new HttpHeaders({'content-type':'application/json'})
  };

  constructor(  http : HttpClient) { 
    // super('https://reqres.in/api/users?page=1', http);
  }

  getUsers(page): Observable <UserDataModel>{
    let theParams: HttpParams= new HttpParams();
    theParams=params.ser('page',page);
    return this.http.get()<UserDataModel>(environment.apiUrl+this.userUrl,{params:theParams});
  }

  hideBtn(){
    .btn-primary{display: this.hideBtn;}
  }
}
