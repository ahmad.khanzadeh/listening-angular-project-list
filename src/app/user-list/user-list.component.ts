import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
// import{HttpClient } from '@angular/common/http';
import{UserListService} from '../user-list.service'
// import {ّّInjectable} from '@angular/core';
// import 'rxjs/add/operator/catch';
// import {Observable} from 'rxjs/observable';

const routes: Routes=[
    {path: '', redirectTo:'/users', pathMatch:'ful'},
];

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  // posts: any[];
  // constructor(http : HttpClient ) { 
  //   http.get('https://reqres.in/api/users?page=1')
  //     .subscribe(response => {this.posts=response;});
  // }
  users: any[];
  persons: any[];
  hide : boolean= true;
  constructor( private service: UserListService){ }

  ngOnInit(page){
    this.getUsers(page).subscribe(users => this.users=splice(0,0,users))
    // this.service.getAll().subscribe(users => this.users=users;)
  };

  getUsers(){
    this.service.getUsers().subscribe(response => {if(response){this.users=response}else{this.service.hideBtn()}});
    }})
    // این this.users رو می تونیم هر جایی که خواستیم فراخوانی کنیم و استفاده کنیم
  }
  show(): void{
    this.hide=!this.hide;
    console.log('Button was clicked for more information')
  }

}
